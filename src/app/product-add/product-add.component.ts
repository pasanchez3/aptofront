import { Component, OnInit, Input  } from '@angular/core';
import { ProductService } from '../product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../model/models';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  @Input() productData = Product;

  constructor(public rest:ProductService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }
  addProduct() {
    this.rest.addProduct(this.productData).subscribe((result) => {
      //this.router.navigate(['/product-details/'+result.id]);
    }, (err) => {
      console.log(err);
    });
  }


}
