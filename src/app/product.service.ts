import { ApiResponse } from './model/apiResponse';
import { Product } from './model/product';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  body: Product[];
  endpoint = 'http://localhost:9000/api/apto/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  constructor(private http: HttpClient) { }
  private extractData(res: ApiResponse) {
    this.body = res.data;
    return this.body || { };
  }
  getProducts(): Observable<any> {
    return this.http.get(this.endpoint + 'products/0').pipe(
      map(this.extractData));
  }
  
  getProduct(id): Observable<any> {
    console.log(id + "idxD");
    return this.http.get(this.endpoint + 'product/' + id).pipe(
      map(this.extractData));
  }
  
  addProduct (product): Observable<any> {
    this.body = product;
    let state = Product.state.Activo;
    this.body.state = state;
    let tipo = Product.type.Servicio;
    this.body.type = tipo;
    return this.http.post<any>(this.endpoint + 'product', JSON.stringify(this.body), this.httpOptions).pipe(
      tap((product) => console.log(`added product w/ id=${product.id}`)),
      catchError(this.handleError<any>('addProduct'))
    );
  }
  
  updateProduct (id, product): Observable<any> {
    this.body = product;
    let state = Product.state.Activo;
    this.body.state = state;
    let tipo = Product.type.Servicio;
    this.body.type = tipo;
    this.body.id = id;
    return this.http.put(this.endpoint + 'product', JSON.stringify(product), this.httpOptions).pipe(
      tap(_ => console.log(`updated product id=${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }
  
  deleteProduct (id): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'product/' + id,this. httpOptions).pipe(
      tap(_ => console.log(`deleted product id=${id}`)),
      catchError(this.handleError<any>('deleteProduct'))
    );
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
